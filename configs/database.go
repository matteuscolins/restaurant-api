package configs

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"restaurant-backend/models"
)

func InitDatabase() (*gorm.DB) {
	db, err := gorm.Open("sqlite3", "restaurant.db")
	if err != nil {
		panic("Error to create database")
	}
	db.AutoMigrate(&models.Cooker{})
	db.AutoMigrate(&models.Order{})
	return db
}
