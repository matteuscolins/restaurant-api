package routes

import (
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"net/http"
	"restaurant-backend/resources"
)

func Routes(router *mux.Router, conn *gorm.DB){

	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		resources.Index(w, r)
	}).Methods("GET")

	// Routing Cooker Resources
	router.HandleFunc("/cooker", func(w http.ResponseWriter, r *http.Request) {
			resources.CreateCooker(conn, w, r)
	}).Methods("POST")

	router.HandleFunc("/cooker", func(w http.ResponseWriter, r *http.Request) {
		resources.FindAllCookers(conn, w, r)
	}).Methods("GET")

	router.HandleFunc("/cooker/{id}", func(w http.ResponseWriter, r *http.Request) {
		resources.FindCookerById(conn, w, r)
	}).Methods("GET")

	router.HandleFunc("/cooker/{id}", func(w http.ResponseWriter, r *http.Request) {
		resources.DeleteCooker(conn, w, r)
	}).Methods("DELETE")

	router.HandleFunc("/cooker/{id}", func(w http.ResponseWriter, r *http.Request) {
		resources.UpdateCooker(conn, w, r)
	}).Methods("PUT")

	// Routing Order Resources
	router.HandleFunc("/order", func(w http.ResponseWriter, r *http.Request) {
		resources.CreateOrder(conn, w, r)
	}).Methods("POST")

	router.HandleFunc("/order", func(w http.ResponseWriter, r *http.Request) {
		resources.FindAllOrders(conn, w)
	}).Methods("GET")

	router.HandleFunc("/order/{id}", func(w http.ResponseWriter, r *http.Request) {
		resources.FindOrderById(conn, w, r)
	}).Methods("GET")

	router.HandleFunc("/order/status/{status_order}", func(w http.ResponseWriter, r *http.Request) {
		resources.FindOrderByStatus(conn, w, r)
	}).Methods("GET")

	router.HandleFunc("/order/{id}", func(w http.ResponseWriter, r *http.Request) {
		resources.DeleteOrder(conn, w, r)
	}).Methods("DELETE")

	router.HandleFunc("/order/{id}", func(w http.ResponseWriter, r *http.Request) {
		resources.UpdateOrder(conn, w, r)
	}).Methods("PUT")

	// Routing Core Resources
	router.HandleFunc("/core", func(w http.ResponseWriter, r *http.Request) {
		resources.TotalCapacity(conn, w, r)
	}).Methods("GET")

	router.HandleFunc("/core/progress", func(w http.ResponseWriter, r *http.Request) {
		resources.OrdersInProgress(conn)
	}).Methods("GET")


}
