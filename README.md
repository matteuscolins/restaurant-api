# restaurant-api
Api para a simulação básica de um restaurante

### Observação
Não consegui comtemplar todos os requisitos pedidos no desafio

### Tecnologias

    -   Go 1.14
    -   GORM (Orm para Golang)
    -   SQLite
    -   Gorilla Mux
    -   Docker
    -   GoLand IDE

### Execução do Projeto (Docker ou Diretamente)
#### Docker
```
docker-compose build && docker-compose up
```
#### Diretamente
```
go run main.go
```

### Collections (Insomnia)
Dentro da pasta `file_insomnia` tem collection para importar e realizar os testes
    