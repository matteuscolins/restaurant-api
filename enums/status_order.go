package enums

type StatusOrder int

const (
	RECEIVED = 0
	QUEUE = 1
	PROGRESS = 2
	READY = 3
	DELIVERED = 4
)
