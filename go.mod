module restaurant-backend

go 1.14

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/gorm v1.9.15
	github.com/jinzhu/inflection v1.0.1-0.20200624021413-970f05d9c0e1 // indirect
	github.com/mattn/go-sqlite3 v1.14.1-0.20200721080010-4c2df3cc4614 // indirect
	github.com/rs/cors v1.7.0
	golang.org/x/crypto v0.0.0-20191205180655-e7c4368fe9dd
)
