FROM golang:latest

ADD . /go/src/restaurant-backend

WORKDIR /go/src/restaurant-backend

RUN go get github.com/tools/godep \
&& go install restaurant-backend \
&& go build /go/src/restaurant-backend/main.go

CMD ["./bin/restaurant-backend"]
