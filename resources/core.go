package resources

import (
	"encoding/hex"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/sha3"
	"net/http"
	"restaurant-backend/enums"
	"restaurant-backend/models"
	"time"
)


func Index(w http.ResponseWriter, r *http.Request) error {
	return json.NewEncoder(w).Encode("Status 200: OK")
}

// TotalCapacity godoc
// @Summary Get total capacity
// @Description Get total capacity kitchen
// @Produce  json
// @Success 200 {int}
// @Failure 400 {string} "Cooker not created"
// @Failure 500 {object} httputil.HTTPError
// @Router /core [get]
func TotalCapacity(db *gorm.DB, w http.ResponseWriter, r *http.Request) int {
	var total = 0
	var cookers []models.Cooker
	if err := db.Find(&cookers).Error; err != nil {
		//return json.NewEncoder(w).Encode("Cookers not found!")
	}
	if len(cookers) > 0 {
		for _, cooker := range cookers {
			if cooker.Status == enums.ENABLED {
				total += cooker.Capacity
			}
		}
		return total
	} else {
		return 0
	}
}

// OrdersInProgress godoc
// @Summary Engine for orders in progress
// @Description Engine to check when orders are in progress
//			and update the status to ready
//			when the preparation time exceeds
//			Obs.: Engine not completed
// @Produce  json
// @Failure 500 {object} httputil.HTTPError
// @Router /core/progess [get]
func OrdersInProgress(db *gorm.DB) int {
	var orders []models.Order
	for {
		if err := db.Find(&orders).Error; err != nil {
			return 0
		}
		if len(orders) > 0 {
			var ordersTemp []models.Order
			for _, order := range orders {
				if order.Status == enums.PROGRESS {
					ordersTemp = append(ordersTemp, order)
				}
			}
			if len(ordersTemp) > 0{
				for _, order := range ordersTemp {
					if time.Now().Local().After(order.UpdatedAt.Add(time.Second * time.Duration(order.PreparationDuration))){
						buf := []byte(string(order.ID)+order.Description+order.ClientName)
						h := make([]byte, 256)
						sha3.ShakeSum256(h, buf)
						order.Status = enums.READY
						order.HashDelivery = hex.EncodeToString(h)
						db.Save(order)
					}
				}
			}
			time.Sleep(time.Second)
			//return len(ordersTemp)
		}
	}
	//return 0
}




