package resources

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"net/http"
	"restaurant-backend/enums"
	"restaurant-backend/models"
)

// CreateCooker godoc
// @Summary Create a cooker
// @Description Create a cooker
// @Accept  json
// @Produce  json
// @Param name 		string 	true "name"
// @Param capacity 	string 	true "capacity"
// @Param status 	int 	true "status"
// @Success 200 {string} "Cooker created"
// @Failure 400 {string} "Cooker not created"
// @Failure 500 {object} httputil.HTTPError
// @Router /cooker [post]
func CreateCooker(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var cooker models.Cooker
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Erro na leitura dos dados")
	}
	json.Unmarshal(reqBody, &cooker)
	if cooker.Status == enums.DISABLED || cooker.Status == enums.ENABLED {
		if err := db.Create(&models.Cooker{Name: cooker.Name, Capacity: cooker.Capacity, Status: cooker.Status}).Error; err != nil {
			return json.NewEncoder(w).Encode("Cooker not created!")
		} else {
			return json.NewEncoder(w).Encode("Cooker created!")
		}
	} else {
		return json.NewEncoder(w).Encode("Error -> Status -> Enter 0 for Disabled or 1 for Enabled.")
	}

}

// FindAllCookers godoc
// @Summary Find all cookers
// @Description Get all cookers
// @Produce  	json
// @Success 200 {array} models.Cooker
// @Failure 404 {string} "Cookers not found"
// @Failure 500 {object} httputil.HTTPError
// @Router /cooker [get]
func FindAllCookers(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var cookers []models.Cooker
	if err := db.Find(&cookers).Error; err != nil {
		return json.NewEncoder(w).Encode("Cookers not found!")
	}
	return json.NewEncoder(w).Encode(cookers)
}

// FindCookerById godoc
// @Summary Find cooker by id
// @Description Get cooker by id
// @Produce  json
// @Param name 		int 	true "id"
// @Success 200 {array} models.Cooker
// @Failure 404 {string} "Cooker not found"
// @Failure 500 {object} httputil.HTTPError
// @Router /cooker/:id [get]
func FindCookerById(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var cooker models.Cooker
	id := mux.Vars(r)["id"]
	if &id != nil {
		if err := db.First(&cooker, id).Error; err != nil {
			return json.NewEncoder(w).Encode("Cooker not found!")
		} else {
			return json.NewEncoder(w).Encode(cooker)
		}
	}
	return json.NewEncoder(w).Encode("Cooker id not found!")
}

// DeleteCooker godoc
// @Summary Delete a cooker
// @Description Delete a cooker
// @Produce  		json
// @Param name 		int 	true "id"
// @Success 200 {string} "Cooker deleted"
// @Failure 400 {string} "Cooker not deleted"
// @Failure 500 {object} httputil.HTTPError
// @Router /cooker/:id [delete]
func DeleteCooker(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var cooker models.Cooker
	id := mux.Vars(r)["id"]
	if &id != nil {
		if err := db.First(&cooker, id).Error; err != nil {
			return json.NewEncoder(w).Encode("Cooker not found!")
		} else {
			if err := db.Delete(&cooker).Error; err != nil {
				return json.NewEncoder(w).Encode("Server error")
			} else {
				return json.NewEncoder(w).Encode("Cooker deleted")
			}
		}
	}
	return json.NewEncoder(w).Encode("Cooker id not found!")
}

// UpdateCooker godoc
// @Summary Edit a cooker
// @Description Edit a cooker
// @Produce  json
// @Produce  			json
// @Param id 			int 	true "id"
// @Param name 			string 	true "name"
// @Param capacity 		int 	true "capacity"
// @Param status 		int 	true "status"
// @Success 200 {string} "Cooker updated"
// @Failure 400 {string} "Cooker not updated"
// @Failure 500 {object} httputil.HTTPError
// @Router /cooker/:id [put]
func UpdateCooker(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var cooker models.Cooker
	var datas models.Cooker
	id := mux.Vars(r)["id"]
	reqBody, err := ioutil.ReadAll(r.Body)
	json.Unmarshal(reqBody, &datas)
	if err != nil {
		fmt.Fprintf(w, "Erro na leitura dos dados")
	}
	if &id != nil {
		if db.First(&cooker, id).RecordNotFound(){
			return json.NewEncoder(w).Encode("Cooker not found!")
		}
		if err := db.First(&cooker, id).Error; err != nil {
			return json.NewEncoder(w).Encode("Receiving data error")
		}
		db.First(&cooker, id)
		if datas.Status == enums.ENABLED {
			cooker.Name = datas.Name
			cooker.Capacity = datas.Capacity
			cooker.Status = enums.ENABLED
			db.Save(&cooker)
			return json.NewEncoder(w).Encode("Cooker updated")
		} else if datas.Status == enums.DISABLED {
			cooker.Name = datas.Name
			cooker.Capacity = datas.Capacity
			cooker.Status = enums.DISABLED
			db.Save(&cooker)
			return json.NewEncoder(w).Encode("Cooker updated")
		} else {
			return json.NewEncoder(w).Encode("Error -> Status -> Enter 0 for Disabled or 1 for Enabled.")
		}
	}
	return nil
}