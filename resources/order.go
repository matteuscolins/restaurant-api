package resources

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/sha3"
	"io/ioutil"
	"net/http"
	"restaurant-backend/enums"
	"restaurant-backend/models"
)

// CreateOrder godoc
// @Summary Create a order
// @Description Create a order
// @Accept  json
// @Produce  json
// @Param name 					string 	true "description"
// @Param capacity 				string 	true "client_name"
// @Param preparation_duration 	string 	true "preparation_duration"
// @Param status				int		true "status"
// @Success 200 {string} "Order created"
// @Failure 400 {string} "Order not created"
// @Failure 500 {object} httputil.HTTPError
// @Router /order [post]
func CreateOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var order models.Order
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Erro na leitura dos dados")
	}
	json.Unmarshal(reqBody, &order)
	if err := db.Create(&models.Order{
		HashReceived:        uuid.New(),
		Description:         order.Description,
		ClientName:          order.ClientName,
		PreparationDuration: order.PreparationDuration,
		Status:              enums.RECEIVED,
		HashDelivery:        "",
	}).Error; err != nil {
		return json.NewEncoder(w).Encode("Order not created!")
	}
	return json.NewEncoder(w).Encode("Order created!")
}

// FindAllOrders godoc
// @Summary Find all orders
// @Description Get all orders
// @Produce  	json
// @Success 200 {array} models.Order
// @Failure 404 {string} "Orders not found"
// @Failure 500 {object} httputil.HTTPError
// @Router /order [get]
func FindAllOrders(db *gorm.DB, w http.ResponseWriter) error {
	var orders []models.Order
	if err := db.Find(&orders).Error; err != nil {
		return json.NewEncoder(w).Encode("Orders not found!")
	}
	return json.NewEncoder(w).Encode(orders)
}

// FindOrderById godoc
// @Summary Find order by id
// @Description Get order by id
// @Produce  json
// @Param name 		int 	true "id"
// @Success 200 {array} models.Order
// @Failure 404 {string} "Order not found"
// @Failure 500 {object} httputil.HTTPError
// @Router /order/:id [get]
func FindOrderById(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var order models.Order
	id := mux.Vars(r)["id"]
	if &id != nil {
		if err := db.First(&order, id).Error; err != nil {
			return json.NewEncoder(w).Encode("Order not found!")
		} else {
			return json.NewEncoder(w).Encode(order)
		}
	}
	return json.NewEncoder(w).Encode("Order id not found!")
}

// FindOrderByStatus godoc
// @Summary Find order by status
// @Description Get order by status
// @Produce  		json
// @Param name 		int 	true "status_code"
// @Success 200 {array} models.Order
// @Failure 404 {string} "Orders not found"
// @Failure 500 {object} httputil.HTTPError
// @Router /order/status/:status_code [get]
func FindOrderByStatus(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var orders []models.Order
	status_order := mux.Vars(r)["status_order"]
	if err := db.Where("status = ?", status_order).Find(&orders).Error; err != nil {
		return json.NewEncoder(w).Encode("Orders not found!")
	}
	return json.NewEncoder(w).Encode(orders)
}

// DeleteOrder godoc
// @Summary Delete a order
// @Description Delete a order
// @Produce  		json
// @Param name 		int 	true "id"
// @Success 200 {string} "Order deleted"
// @Failure 400 {string} "Order not deleted"
// @Failure 500 {object} httputil.HTTPError
// @Router /order/:id [delete]
func DeleteOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var order models.Order
	id := mux.Vars(r)["id"]
	if &id != nil {
		if err := db.First(&order, id).Error; err != nil {
			return json.NewEncoder(w).Encode("Order not found!")
		} else {
			if err := db.Delete(&order).Error; err != nil {
				return json.NewEncoder(w).Encode("Server error")
			} else {
				return json.NewEncoder(w).Encode("Order deleted")
			}
		}
	}
	return json.NewEncoder(w).Encode("Order id not found!")

}

// UpdateOrder godoc
// @Summary Edit a order
// @Description Edit a order
// @Accept  json
// @Produce  json
// @Produce  			json
// @Param id 					int 	true "id"
// @Param name 					string 	true "description"
// @Param capacity 				int 	true "client_name"
// @Param preparation_duration 	int 	true "preparation_duration"
// @Param status 				int 	true "status"
// @Success 200 {string} "Order updated"
// @Failure 400 {string} "Order not updated"
// @Failure 500 {object} httputil.HTTPError
// @Router /order/:id [put]
func UpdateOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) error {
	var order models.Order
	var datas models.Order
	id := mux.Vars(r)["id"]
	reqBody, err := ioutil.ReadAll(r.Body)
	json.Unmarshal(reqBody, &datas)
	if err != nil {
		fmt.Fprintf(w, "Erro na leitura dos dados")
	}
	if &id != nil {
		if db.First(&order, id).RecordNotFound(){
			return json.NewEncoder(w).Encode("Order not found!")
		}
		if err := db.First(&order, id).Error; err != nil {
			return json.NewEncoder(w).Encode("Receiving data error")
		}
		db.First(&order, id)
		if datas.Status != enums.DELIVERED {
			order.Description = datas.Description
			order.ClientName = datas.ClientName
			order.PreparationDuration = datas.PreparationDuration
			order.Status = datas.Status
			db.Save(&order)
			return json.NewEncoder(w).Encode("Order updated")
		} else {
			buf := []byte(order.Description)
			h := make([]byte, 256)
			sha3.ShakeSum256(h, buf)
			order.Description = datas.Description
			order.ClientName = datas.ClientName
			order.PreparationDuration = datas.PreparationDuration
			order.Status = datas.Status
			order.HashDelivery = hex.EncodeToString(h)
			db.Save(&order)
			return json.NewEncoder(w).Encode("Order updated")
		}
	}
	return nil
}