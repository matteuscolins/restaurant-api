package main

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"log"
	"net/http"
	"restaurant-backend/configs"
	"restaurant-backend/routes"
)

func main()  {
	conn := configs.InitDatabase()
	router := mux.NewRouter()
	routes.Routes(router, conn)
	handler := cors.AllowAll().Handler(router)
	log.Fatal(http.ListenAndServe(":5000", handler))
}
