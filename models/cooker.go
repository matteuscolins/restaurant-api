package models

import (
	"github.com/jinzhu/gorm"
)


type Cooker struct {
	gorm.Model
	Name 		string 		`json:"name"`
	Capacity	int 		`json:"capacity"`
	Status      int			`json:"status"`         
}
