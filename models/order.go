package models

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

type Order struct {
	gorm.Model
	HashReceived			uuid.UUID		`json:"hash_received"`
	Description 			string 			`json:"description"`
	ClientName 				string 			`json:"client_name"`
	PreparationDuration 	int 			`json:"preparation_duration"`
	Status 					int 			`json:"status"`
	HashDelivery 			string			`json:"hash_delivery"`
}
